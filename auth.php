<?php 
if(!defined('DOKU_INC')) die();
/**
 * NCSU WRAP authentication building off of authplain
 *
 * @license    GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author     Matt Haught <dmhaught@ncsu.edu>
 */
class auth_plugin_authwrap extends auth_plugin_authplain {
 
    function __construct() {
        parent::__construct();
        global $config_cascade;

        if(!@is_readable($config_cascade['plainauth.users']['default'])) {
            $this->success = false;
        } else {
            if(@is_writable($config_cascade['plainauth.users']['default'])) {
                $this->cando['addUser']   = true;
                $this->cando['delUser']   = true;
                $this->cando['modLogin']  = true;
                $this->cando['modPass']   = true;
                $this->cando['modName']   = true;
                $this->cando['modMail']   = true;
                $this->cando['modGroups'] = true;
            }
            $this->cando['getUsers']     = true;
            $this->cando['getUserCount'] = true;
        }
        $this->cando['external'] = true;
        $this->cando['logout']   = true;
    }
 
    function trustExternal($user, $pass, $sticky = false) {
 
        global $USERINFO;
        global $conf;
 
        $sticky ? $sticky = true : $sticky = false; //sanity check
 
        if (!empty($_SERVER['WRAP_USERID'])) {
            // check is already local user, and add if not
            if ($this->getUserData($_SERVER['WRAP_USERID']) === false) {
                $ldapds = ldap_connect('ldap.ncsu.edu', 389);
                $ldapres = ldap_bind($ldapds);
                $ldapsearch = @ldap_search($ldapds, 'ou=accounts,dc=ncsu,dc=edu', 'uid=' . $_SERVER['WRAP_USERID'], array('cn', 'title', 'uid', 'ncsutwopartname'), 0, 1);
                $ldapresults = @ldap_get_entries($ldapds, $ldapsearch);
                if (!empty($ldapresults[0]['ncsutwopartname'][0])) {
                    $username = $ldapresults[0]['ncsutwopartname'][0];
                } else {
                    $username = $_SERVER['WRAP_USERID'];
                }
                $this->createUser($_SERVER['WRAP_USERID'],
                    md5(rand(10,10)),
                    $username,
                    $_SERVER['WRAP_USERID'].'@ncsu.edu',
                    'user'
                );
            }
            $userinfo = $this->getUserData($_SERVER['WRAP_USERID']);
            if ($userinfo !== false) {
                $USERINFO['name'] = $userinfo['name'];
                $USERINFO['mail'] = $userinfo['mail'];
                $USERINFO['grps'] = $userinfo['grps'];
                $_SESSION[DOKU_COOKIE]['auth']['user'] = $_SERVER['WRAP_USERID'];
                $_SESSION[DOKU_COOKIE]['auth']['pass'] = '';
                $_SESSION[DOKU_COOKIE]['auth']['buid'] = auth_browseruid();
                $_SESSION[DOKU_COOKIE]['auth']['info'] = $userinfo;
                $_SESSION[DOKU_COOKIE]['auth']['time'] = time();
                return true;
            }
            return false;
        } else {
            return false;
        }
    }


    function logOff () {
        echo "<meta http-equiv=\"refresh\" 
            content=\"0;URL='https://webauth.ncsu.edu/wrap-bin/was16.cgi?logout'\">";
        exit('Logoff failed, 
            please manually 
            <a href="https://webauth.ncsu.edu/wrap-bin/was16.cgi?logout">LOGOUT</a>.'
        );
    }

}
